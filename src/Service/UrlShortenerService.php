<?php

namespace App\Service;

use App\Exception\NotFoundException;
use App\Repository\UrlEntryRepository;
use App\Entity\UrlEntry;
use Doctrine\ORM\EntityManagerInterface;

class UrlShortenerService
{

    const RANDOM_STRING = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const MAX_RANDOM_STRING = 10;

    /** @var UrlEntryRepository $urlEntryRepository  */
    private $urlEntryRepository;

    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * UrlShortenerService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->urlEntryRepository = $entityManager->getRepository(UrlEntry::class);
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $url
     * @return string
     * @throws NotFoundException
     */
    public function buildShortUrl(string $url) : string
    {
        if ($randomCode = $this->getUrlEntryByLongUrl($url)) {
            return $randomCode;
        } else {
            if ($this->checkUrl($url)) {
                return $this->generateShortUrl($url);
            } else {
                throw new NotFoundException('Web Site does not exist.');
            }
        }
    }

    /**
     * @param string $url
     * @return bool
     * @throws \ErrorException
     */
    public function checkUrl(string $url) : bool
    {
        try {
            file_get_contents($url);
        } catch (\Exception $exception) {
            return false;
        }
        return true;
    }

    /**
     * @param string $url
     * @return string
     */
    protected function generateShortUrl(string $url) : string
    {
        $randomCode = $this->generateRandomCode();
        $urlEntry = new UrlEntry();
        $urlEntry->setLongUrl($url)
            ->setRandomCode($randomCode)
            ->setDateCreated(new \DateTime());

        $this->entityManager->persist($urlEntry);
        $this->entityManager->flush();
        return $randomCode;
    }

    /**
     * @param string $url
     * @return string
     */
    protected function getUrlEntryByLongUrl(string $url) : string
    {
        $entry = $this->urlEntryRepository->findOneByLongUrl($url);
        return ($entry ? $entry->getRandomCode() : '');
    }

    /**
     * @param string $randomCode
     * @return string
     */
    public function getUrlEntryByRandomCode(string $randomCode) : string
    {
        $entry = $this->urlEntryRepository->findOneByRandomCode($randomCode);
        return ($entry ? $entry->getLongUrl() : '');
    }

    /**
     * @return string
     */
    protected function generateRandomCode() : string
    {
        $characters = self::RANDOM_STRING;
        $randomStringLength = strlen($characters);
        $length = mt_rand(1, self::MAX_RANDOM_STRING);
        $generatedCode = '';
        for ($i = 0; $i < $length; $i++) {
            $generatedCode .= $characters[rand(0, $randomStringLength - 1)];
        }
        if ($this->getUrlEntryByRandomCode($generatedCode)) {
            return $this->generateRandomCode();
        } else {
            return $generatedCode;
        }
    }

}

