<?php

namespace App\Controller;

use ErrorException;
use App\Exception\NotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Service\UrlShortenerService;

class UrlShortnerController extends Controller
{

    const HEADER_PERMANENT = 301;
    const HEADER_NOT_FOUND = 404;
    const HEADER_EXCEPTION = 500;

    const SHORT_URL_PREFIX = '/';
    /**
     * @var UrlShortenerService
     */
    private $urlShortnerService;


    public function __construct(UrlShortenerService $urlShortenerService)
    {
        $this->urlShortnerService = $urlShortenerService;
    }

    /**
     * @Route("/{slug}", name="url_shortner", methods="GET")
     *
     * @param $slug
     * @return JsonResponse|RedirectResponse
     */
    public function indexAction($slug)
    {
        try {
            $url = $this->urlShortnerService->getUrlEntryByRandomCode($slug);
        } catch (ErrorException $e) {
            if ($e instanceof NotFoundException){
                $header = self::HEADER_NOT_FOUND;
            } else {
                $header = self::HEADER_EXCEPTION;
            }
            return $this->json([
                $e->getMessage()
            ], $header);
        }
        return $this->redirect($url, self::HEADER_PERMANENT);
    }

    /**
     * @Route("/", name="url_shortner_generate", methods="POST")
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \ErrorException
     */
    public function generateAction(Request $request)
    {
        try {
            error_log(var_export($request->request->all(), true));
            $url = $this->urlShortnerService->buildShortUrl($request->request->get('url'));
        } catch (ErrorException $e) {
            if ($e instanceof NotFoundException){
                $header = self::HEADER_NOT_FOUND;
            } else {
                $header = self::HEADER_EXCEPTION;
            }
            return $this->json([
                'Error' => $e->getMessage()
            ], $header);
        }

        return $this->json([
            'short_url' => self::SHORT_URL_PREFIX . $url,

        ]);
    }
}
