<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UrlEntryRepository")
 */
class UrlEntry
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $long_url;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $random_code;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_created;

    public function getId()
    {
        return $this->id;
    }

    public function getLongUrl(): ?string
    {
        return $this->long_url;
    }

    public function setLongUrl(string $long_url): self
    {
        $this->long_url = $long_url;

        return $this;
    }

    public function getRandomCode(): ?string
    {
        return $this->random_code;
    }

    public function setRandomCode(string $random_code): self
    {
        $this->random_code = $random_code;

        return $this;
    }

    public function getDateCreated(): ?\DateTimeInterface
    {
        return $this->date_created;
    }

    public function setDateCreated(\DateTimeInterface $date_created): self
    {
        $this->date_created = $date_created;

        return $this;
    }
}
