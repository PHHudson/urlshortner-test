<?php

namespace App\Exception;

use ErrorException;

class NotFoundException extends ErrorException
{
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}