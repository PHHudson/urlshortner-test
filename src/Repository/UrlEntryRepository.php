<?php

namespace App\Repository;

use App\Entity\UrlEntry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UrlEntry|null find($id, $lockMode = null, $lockVersion = null)
 * @method UrlEntry|null findOneBy(array $criteria, array $orderBy = null)
 * @method UrlEntry[]    findAll()
 * @method UrlEntry[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UrlEntryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UrlEntry::class);
    }

    /**
     * @param string $randomCode
     * @return UrlEntry
     */
    public function findOneByRandomCode(string $randomCode)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.random_code = :val')
            ->setParameter('val', $randomCode)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param string $url
     * @return UrlEntry
     */
    public function findOneByLongUrl(string $url)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.long_url = :val')
            ->setParameter('val', $url)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
