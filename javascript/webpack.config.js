const path = require('path');

module.exports = {
    entry: './src/index.js',
    output: {
        path: path.resolve('../public/dist'),
        filename: 'index_bundle.js'
    },
    module: {
        rules: [
            { test: /\.s?css$/,
                use: [
                    { loader: "style-loader" },
                    { loader: "css-loader" },
                    { loader: "sass-loader" }
                ]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: "babel-loader"
            }, {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: "babel-loader"
            }
        ]
    }
}