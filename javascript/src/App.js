import React, { Component } from "react";
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import UrlSection from './components/UrlSection/UrlSection';


import './App.scss';

class App extends Component {

    render() {
        return (
            <div className="app-wrapper">
                <Paper className="App" elevation={4}>
                    <Typography variant="headline" component="h3">
                        ACME URL Generator
                    </Typography>
                    <UrlSection />
                </Paper>
            </div>
        );
    }
}

export default App;

