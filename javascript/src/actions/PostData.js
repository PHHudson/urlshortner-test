import axios from 'axios';


class PostData
{
    sendUrl(url) {
        const data = new FormData();
        data.append('url', url);

        return axios.post('/', data)
            .then(data => {
                if(data.data) {
                    return data.data;
                }
            }).catch(e => {
                if (e.response) {
                    throw e.response.data;
                }
        });
    }
}

export default PostData;

