import React from 'react';

import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import PostData from '../../actions/PostData';

import './styles.scss';



 class UrlSection extends React.Component {

     constructor(props) {
         super(props);
         this.state = {
             value: '',
             url: '',
             message: '',
             error: false,
         };
         this.handleChange = this.handleChange.bind(this);
         this.generateUrl = this.generateUrl.bind(this);
         this.handleError = this.handleError.bind(this);
         this.close = this.close.bind(this);
         this.urlPanel = this.urlPanel.bind(this);
     }

     urlLink = 'http://127.0.0.1:8000';


     handleChange(event) {
         this.setState({value: event.target.value});
     }

    generateUrl() {
        const postData = new PostData();
        postData.sendUrl(this.state.value).then(this.urlPanel).catch(this.handleError);
    }

    urlPanel (result) {
         if (result && result.short_url) {
             const url = this.generateLink(result.short_url);
             this.setState({message: url, error: false});
         }
    }

    generateLink(url) {
         const fullUrl = this.urlLink + url;
         return (<div>Your link has been successfully generated:<br /> <a href={fullUrl} target="_blank">{fullUrl}</a></div>)
    }

     handleError (error) {
         if (error.Error) {
             this.setState({message: error.Error, error: true});
             console.log('result', this.state.message);
         }
     }

     close () {
         this.setState({message: '', error: false});
     }



    render() {
        return (
            <div className="inputs-wrapper">
                {this.state.message ?
                    <SnackbarContent className={this.state.error ? 'error-message' : 'notice-message'} message={this.state.message} action={
                        <IconButton key="close" aria-label="Close" color="inherit" onClick={this.close}><CloseIcon />
                        </IconButton>
                    } />
                    : ''}
                <Grid container spacing={16} justify="center">
                    <Grid item lg>
                        <TextField
                            label="Enter URL to shorten"
                            id="url-input"
                            className="url-text"
                            required={true}
                            value={this.state.value}
                            onChange={this.handleChange}
                        />
                    </Grid>
                    <Grid item lg className="button-wrapper">
                        <Button variant="contained" color="primary" className="button-generate"
                                onClick={this.generateUrl}>Generate URL</Button>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default UrlSection;