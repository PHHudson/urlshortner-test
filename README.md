# Url Shortner

Without using an external database, we'd like you to create a URL shortening service. 

There should be an endpoint that responds to POST with a json body containing a URL, which responds with a JSON repsonse of the short url and the orignal URL, as in the following curl example:

~~~
curl localhost -XPOST -d '{ "url": "http://www.payasugym.com" }'
{ "short_url": "/abc123", "url": "http://www.payasugym.com" }
~~~
When you send a GET request to a previously returned URL, it should redirect to the POSTed URL, as shown in the following curl example:

~~~
curl -v localhost/abc123
...
< HTTP/1.1 301 Moved Permanently
...
< Location: http://www.payasugym.com
...
{ "url": "http://www.payasugym.com" }
~~~
### Welcome to ACME URL Shortner. To operate please follow the following steps:
To run this software you need PHP 7.2
latest version of node/ v9.4.0

1. cd to the project folder (urlshortner-test)
2. copy `.env.dist` to `.env`
3. run `composer install`
4. run `cd javascript`
5. run `npm install` or `yarn install`
6. `npm run build` or `yarn build`
7. cd up a level - `cd ../`
8. Run `php ./bin/console server:run`
9. Finally go to [http://127.0.0.1:8000/index.html](http://127.0.0.1:8000/index.html)

